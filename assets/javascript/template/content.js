// To set the minimum height of content class
$( document ).ready(function() 
{
	$( '.content' ).css( "min-height", $( '.wrapper' ).height() + 'px' );
});

$( window ).resize(function()
{
	// Reset content height to make wrapper calculate the new height
	$( '.content' ).css( "min-height", 0 );
	
	// Get the new wrapper height
	$( '.content' ).css( "min-height", $( '.wrapper' ).height() + 'px' );
});