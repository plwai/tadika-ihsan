$("document").ready(function(){
  $("#class_dropdown").change(function(){
	var base_url = window.location.origin;
	var value = $('#class_dropdown').val();
	
    $.ajax({
        type: "POST",
        dataType: "json",
        url: base_url + "/TadikaIhsan/overview/select",
	    cache: false,
	    data: {'select' : value},
        success: function(data) {
			// Clear the previos chart
		    $("#chart2").html(
            ""
            );
			
		    var ticks = data.name;

			var plot2 = $.jqplot('chart2', [
            data.number],{
			
            seriesDefaults: {
				renderer:$.jqplot.BarRenderer,
            
            pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
            
            shadowAngle: 135,

            rendererOptions: {
                barDirection: 'horizontal'
            }
			},
			axes: {
				yaxis: {
					ticks: ticks,
					renderer: $.jqplot.CategoryAxisRenderer
				}
			}
		});
      }
    });
  });
});
