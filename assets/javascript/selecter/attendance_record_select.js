$("document").ready(function(){
  $("#class_dropdown").change(function(){
  		var value = $('#class_dropdown').val();
  		var select = $('#month_dropdown');
  		var date = new Date();
  		var month = date.getMonth();
  		var year = date.getFullYear();
  		var month_name = ["jan", "feb", "mar", "apr", "may", "june", "july", "aug", "sept", "oct", "nov", "dec"];

  		if(value != year)
  		{
			select.empty().append("<option id='jan' value='1' >Jan</option><option id='feb' value='2' >Feb</option><option id='mar' value='3' >Mar</option><option id='apr' value='4' >Apr</option><option id='may' value='5' >May</option><option id='june' value='6' >June</option><option id='july' value='7' >Jult</option><option id='aug' value='8' >Aug</option><option id='sept' value='9' >Sept</option><option id='oct' value='10' >Oct</option><option id='nov' value='11' >Nov</option><option id='dec' value='12' >Dec</option>");
		}
		else
		{
			select.empty();
			for(var i = 1; i <= month; i++)
			{
				select.append("<option id='" + month_name[i-1] + "' value='" + i + "' >" + month_name[i-1].charAt(0).toUpperCase() + month_name[i-1].slice(1)) + "</option>";
			}
		}

		var base_url = window.location.origin;
		var month = $('#month_dropdown').val();
		var year = $('#class_dropdown').val();
		
	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: base_url + "/TadikaIhsan/attendance_record/get_data",
		    cache: false,
		    data: {'month' : month, 'year' : year},
	        success: function(data) {
				$(".list_select").html(data.contents);
				$('#table_id').dataTable().fnDestroy();
				$('#table_id').dataTable();
	        }
	    });
  });

$("#month_dropdown").change(function(){
	var base_url = window.location.origin;
	var month = $('#month_dropdown').val();
	var year = $('#class_dropdown').val();
	
    $.ajax({
        type: "POST",
        dataType: "json",
        url: base_url + "/TadikaIhsan/attendance_record/get_data",
	    cache: false,
	    data: {'month' : month, 'year' : year},
        success: function(data) {
			$(".list_select").html(data.contents);
			$('#table_id').dataTable().fnDestroy();
			$('#table_id').dataTable();
        }
    });
  });
});


function showDetails(vc){
	var base_url = window.location.origin;
	var month = $('#month_dropdown').val();
  	var year = $('#class_dropdown').val();

	$.ajax({
        type: "POST",
        dataType: "json",
        async: false,
        url: base_url + "/TadikaIhsan/attendance_record/get_details",
	    cache: false,
	    data: {'vc' : vc, 'month' : month, 'year' : year},
        success: function(data) {
		    $(".page_select").html(data.contents);
      	}
    });
}