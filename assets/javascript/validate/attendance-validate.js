var base_url = window.location.origin;

$('form').validate({
        rules: {
            vc_check_in: {
                required: true,
				remote: {
                    url: base_url + "/TadikaIhsan/attendance/check_vc",
                    type: "post"
                }
            },
			vc_check_out: {
                required: true,
				remote: {
                    url: base_url + "/TadikaIhsan/attendance/check_vc",
                    type: "post"
                }
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
			if(element.attr("id") == "vc_check_in"){
				error.insertAfter(".vcError");
			}
			else if(element.attr("id") == "vc_check_out"){
				error.insertAfter(".vcError");
			}
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
