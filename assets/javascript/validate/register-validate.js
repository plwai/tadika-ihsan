var base_url = window.location.origin;

$('form').validate({
        rules: {
            email: {
                required: true,
				remote: {
                    url: base_url + "/TadikaIhsan/login/checkUser",
                    type: "post"
                }
            },
			name: {
                required: true
            },
            password: {
                required: true
            },
			confirmPass:{
				equalTo: "#password",
			}
        },
		messages: {
            email: {
                required: "Please Enter Email!",
                email: "This is not a valid email!",
                remote: "Email already in use!"
            }
		},
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
