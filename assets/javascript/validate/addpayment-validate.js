var base_url = window.location.origin;
var vc = $('#vcno').val();
var month = $('#month_dropdown').val();
var year = $('#class_dropdown').val();
var pay = $('#payvalue').val();
var minimun = 10000000000;

jQuery.validator.addMethod("max_pay", function(value, element) {
       $.ajax({
            type: "POST",
            async: false,
            dataType: "json",
            url: base_url + "/TadikaIhsan/add_payment/check_min",
            cache: false,
            data: {'vc' : $('#vcno').val(), 'month' : $('#month_dropdown').val(), 'year': $('#class_dropdown').val(), 'amount': $('#payvalue').val()},
            success: function(data) {
                minimun = data.result;
            }
        });

        return value < minimun;
}, "The payment is exceeding your balance_fee.");

$('form').validate({
        rules: {
            vcno: {
                required: true,
                remote: {
                    url: base_url +"/TadikaIhsan/add_payment/check_vc",
                    type: "post"
                }
            },
            month: {
                required: true
            },
			payvalue: {
                required: true,
                remote: {
                    url: base_url +"/TadikaIhsan/add_payment/check_amount",
                    type: "post"
                },
                max_pay: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    });
