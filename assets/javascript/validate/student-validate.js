var base_url = window.location.origin;

$('form').validate({
        rules: {
            name: {
                required: true,
            },
			age: {
                required: true
            },
            gender: {
                required: true
            },
			date: {
                required: true
            },
            fee: {
            	required: true
            },
            discount: {
            	required: true
            },
			guardianName: {
                required: true,
            },
			guardianDate: {
                required: true,
            },
			guardianIdentity: {
                required: true,
            },
			guardianOccupation: {
                required: true,
            },
			guardianContact: {
                required: true,
            },
			guardianStreet1: {
                required: true,
            },
			guardianPost: {
                required: true,
            },
			guardianState: {
                required: true,
            },
			guardianCountry: {
                required: true,
            },
			guardianNationality: {
                required: true,
            },
			guardianRelation: {
				required: true
			},
			guardianName2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianRelation2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianDate2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianIdentity2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianOccupation2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianContact2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardian2Street1: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianPost2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianState2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianCountry2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			},
			guardianNationality2: {
				required: {
					depends: function(element){
						if($('#guardianName2').val() != '' || $('#guardianRelation2').val() != null || $('#guardianDate2').val() != '' || $('#guardianIdentity2').val() != '' || $('#guardianOccupation2').val() != '' || $('#guardianContact2').val() != '' || $('#guardian2Street1').val() != '' || $('#guardianPost2').val() != '' || $('#guardianState2').val() != '' || $('#guardianCountry2').val() != '' || $('#guardianNationality2').val() != ''){
							return true;
						}
						else{
							return false;
						}
					}
				}
			}
        },
		messages: {
            email: {
                required: "This field is required"
            }
		},
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
			if(element.attr("id") == "gender"){
				error.insertAfter(".genderError");
			}
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            }
            else if(element.attr("id") == "fee"){
            	error.insertAfter(".feeError");
            }
            else if(element.attr("id") == "discount"){
            	error.insertAfter(".discountError");
            }
            else {
                error.insertAfter(element);
            }
        }
    });
