var base_url = window.location.origin;

$('form').validate({
        rules: {
            name: {
                required: true,
            },
			age: {
                required: true
            },
            gender: {
                required: true
            },
			contact: {
                required: true
            },
            email: {
                required: true
            },
			street1: {
                required: true
            },
			poscode: {
                required: true
            },
			state: {
                required: true
            },
			country: {
                required: true
            },
			nationality: {
                required: true
            },
			icNum:{
				required: true
			}
        },
		messages: {
            email: {
                required: "This field is required"
            }
		},
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
			if(element.attr("id") == "gender"){
				error.insertAfter(".genderError");
			}
            else if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
