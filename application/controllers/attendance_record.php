<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance_record extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
		$this->load->model('attendance_model');
	}
	
	public function index(){
		$data['title'] 	= "Attendance record";
	    
		 if(date('n') == 1)
	    {
	    	$new_data = 12;
	    }
	    else
	    {
	    	$new_data = date('n') - 1;
	    }

	    if(date('n') == 1)
	    {
	    	$data_year = date('Y') - 1;
	    }
	    else
	    {
	    	$data_year = date('Y');
	    }

	    $username = $this->session->userdata('username');
	    
	    $data['dayNum'] = cal_days_in_month(CAL_GREGORIAN, $new_data, $data_year);
	    $data['username'] 	= $username;

	    $data['result'] = $this->attendance_model->get_data($new_data, $data_year);
		
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('attendance_record');
		$this->load->view('template/footer');
	}

	public function get_data(){
		$count = 0;
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$dayNum = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$vc = NULL;

		$result = $this->attendance_model->get_data($month, $year);
		$data['contents'] = "";
		$data['contents'] = "<thead>
								<tr>
									<th>No</th>
									<th>NAME</th>
									<th>STUDENT NUMBER</th>
									<th>NUMBER OF ATTENDANCE</th>
									<th>DETAILS</th>
								</tr>
							</thead>
							<tbody>";

		foreach($result as $item)
		{
			if($item['vc'] != $vc)
			{
				if($vc != NULL)
				{
					$data['contents'] .= "    <th>".$attendance."/".$dayNum."</th>
										   <th><a class='pointer' onclick='showDetails(".$vc.")'>Details</a></th>
										</tr>";
				}

				$vc = $item['vc'];
				$attendance = 0;
				$count++;
				$data['contents'] .= "<tr>
										<th>".$count."</th>
										<th>".$item['name']."</th>
										<th>".$item['vc']."</th>";
			}

			$attendance++;
			
		}

		if(isset($result[0]))
		{
			$data['contents'] .= "    <th>".$attendance."/".$dayNum."</th>
									   <th><a class='pointer' onclick='showDetails(".$vc.")'>Details</a></th>
									</tr>
									</tbody>
									</table>";
		}

		echo json_encode($data);
	}

	public function get_details()
	{
		$count = 0;

		$vc = $this->input->post('vc');
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$dayNum = cal_days_in_month(CAL_GREGORIAN, $month, $year);

		$result = $this->attendance_model->get_details($vc, $month, $year);

		$data['contents'] = "<br><br>
							<div class='row font_bold'>
								<div class='col-sm-2'>
									vc number: 
								</div>
								<div class='col-sm-6'>
									".$result[0]['vc']."
								</div>
							</div>
							<div class='row font_bold'>
								<div class='col-sm-2'>
									Name: 
								</div>
								<div class='col-sm-6'>
									".$result[0]['name']."
								</div>
							</div>
							<div class='empty_box'>
								
							</div>
							<div class='row'>
								<table class='table'>
									<thead>
										<tr>
											<th style='width:200px;'>#</th>
											<th>Date</th>
											<th>Check In(time)</th>
											<th>Check Out(time)</th>
										</tr>
									</thead>
									<tbody>";

		foreach($result as $item)
		{
			$count++;
			$data['contents'] .= "<tr>
									<th>".$count."</th>
									<th>".$item['date']."</th>
									<th>".$item['check_in']."</th>
									<th>".$item['check_out']."</th>
								  </tr>";
		}

		$data['contents'] .= "    <tr>
									<th></th>
									<th></th>
									<th></th>
									<th>Attendance : ".$count."/".$dayNum."</th>
								  </tr>
							  </tbody>
							</table>
							<div class='col-sm-offset-4'>
							<button onclick='javascript:location.reload()' class='btn btn-default btn-lg'>Back</button>
							";
	    
	    echo json_encode($data);
	}
}