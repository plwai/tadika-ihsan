<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Overview extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
	}
	
	public function index()
	{
	    $data['title'] 	= "Overview";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('overview');
		$this->load->view('template/footer');
		
	}
	
	public function select()
	{
		$data["select"] = $this->input->post('select');
		
		// this->load->model->get_class_data($data["select"]);
		$data["name"] = array('Student1', 'Student2', 'Student3', 'Student4', 'Student5', 'Student6', 'Student7', 'Student8', 'Student9', 'Student10');
		
		$data["number"] = array(array(2,1), array(4,2), array(6,3), array(3,4), array(2,5), array(4,6), array(6,7), array(3,8), array(2,9), array(4,10));
		
		echo json_encode( $data );
	}
}