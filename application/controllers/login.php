<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function __construct() {
        parent::__construct();
		
		$this->load->model('login_model');
		
    }
	
	public function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			redirect('home');
		}
		else
		{
			$this->login();
		}
	}
	
	public function login()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
		
			if ($this->form_validation->run())
			{
				$username  = $this->input->post('email');
				$password  = $this->input->post('password');
			
				$_data = $this->login_model->get_users($username);
				
				$enc_pass	= explode(":", $_data["enc_password"]);
				$salt		= $enc_pass[1];
				
				if(md5($password.$salt)!=$enc_pass[0])
				{
					$this->session->set_flashdata('errmsg', 'Invalid username or password.');
					redirect('login');
				}
				
				$data = array(
						'username' 	=> $_data['username'],
						'staff_id'  => $_data['staff_id'],
						'is_logged_in' 	=> true
					);
				$this->session->set_userdata($data);
				redirect('home');
			}
		}
		
		$data['title'] = 'Login';
		$this->load->view('template/header', $data);
		$this->load->view('login');
		$this->load->view('template/footer');
	}
	
	public function checkUser()
	{
		$email    = $this->input->post('email');
		if ($this->login_model->checkUser($email))
		{
			echo "false";          
		} 
		else 
		{
			echo "true";
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/');
	}
}

