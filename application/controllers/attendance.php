<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
		$this->load->model('attendance_model');
	}
	
	public function index(){
		$data['title'] 	= "Attendance";
		$data['function'] = '';
		$data['complete'] = '';
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
		
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('attendance');
		$this->load->view('template/footer');
	}
	
	public function check_in()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{	
			$username = $this->session->userdata('username');
	    
			$data['username'] 	= $username;
			
			$now = time();
			$date = date ("Y-m-d", $now);
			$time = date ("G:i:s", $now);

			if($this->input->post('vc_check_in') AND $this->attendance_model->check_in_duplicate($this->input->post('vc_check_in')))
			{
				$this->attendance_model->check_in($this->input->post('vc_check_in'), $this->session->userdata('staff_id'));
				$data['title'] 	= "Attendance";
				$data['function'] = "check_in";
				$data['complete'] = 'Checked in at '.$date.' '.$time;
				
				$this->load->view('template/header', $data);
				$this->load->view('template/sidebar', $data);
				$this->load->view('attendance');
				$this->load->view('template/footer');
			}
			else
			{
				$data['title'] 	= "Attendance";
				$data['function'] = "check_in";
				
				$this->load->view('template/header', $data);
				$this->load->view('template/sidebar', $data);
				$this->load->view('check_in_out');
				$this->load->view('template/footer');
			}
		}
	}
	
	public function check_out()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$username = $this->session->userdata('username');
	    
			$data['username'] = $username;

			$now = time();
			$date = date ("Y-m-d", $now);
			$time = date ("G:i:s", $now);
			
			if($this->input->post('vc_check_out') AND $this->attendance_model->check_out_duplicate($this->input->post('vc_check_out')))
			{
				$this->attendance_model->check_out($this->input->post('vc_check_out'), $this->session->userdata('staff_id'));
				
				$data['title'] 	= "Attendance";
				$data['function'] = "check_out";
				$data['complete'] = 'Checked out at '.$date.' '.$time;
				
				$this->load->view('template/header', $data);
				$this->load->view('template/sidebar', $data);
				$this->load->view('attendance');
				$this->load->view('template/footer');
			}
			else
			{
				$data['title'] 	= "Attendance";
				$data['function'] = "check_out";
				
				$this->load->view('template/header', $data);
				$this->load->view('template/sidebar', $data);
				$this->load->view('check_in_out');
				$this->load->view('template/footer');
			}
		}
	}
	
	public function check_vc()
	{
		if($this->input->post('vc_check_in'))
		{
			$vc = $this->input->post('vc_check_in');
			if ($this->attendance_model->check_vc($vc))
			{
				if($this->attendance_model->check_in_duplicate($vc))
				{
					echo "true"; 
				}
				else   
				{
					echo json_encode("You have already checked in.");
				}
			} 
			else 
			{
				echo json_encode("The vc is not existed.");
			}
		}
		else
		{
			$vc = $this->input->post('vc_check_out');
			if ($this->attendance_model->check_vc($vc))
			{
				if($this->attendance_model->check_out_duplicate($vc))
				{
					if($this->attendance_model->check_out_validate($vc))
					{
						echo "true"; 
					}
					else
					{
						echo json_encode("You have already checked out.");
					}
				}
				else   
				{
					echo json_encode("You havent checked in.");
				}
			} 
			else 
			{
				echo json_encode("The vc is not existed.");
			}
		}
	}
}