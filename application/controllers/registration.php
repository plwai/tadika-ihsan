<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
		$this->load->model('login_model');
	}
	
	public function index()
	{
	    $data['title'] 	= "Registration";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('registration');
		$this->load->view('template/footer');
		
	}
	
	
	public function register()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$this->form_validation->set_rules('email', 'email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			
			if ($this->form_validation->run())
			{
				$username  = $this->input->post('email');
				$password  = $this->input->post('password');
				
				$salt = $this->generate_random_code(32);
				$enc_password 	= md5($password.$salt).":".$salt;
				
				$info = array(
					'username'		=> $username,	
					'password'      => $password,
					'enc_password'  => $enc_password
				);
				
				$this->login_model->register($info);
				
				redirect('registration');
			}
		}
		
		$data['title'] = 'Sign up';
		$this->load->view('template/header', $data);
		$this->load->view('registration');
		$this->load->view('template/footer');
	}
	
	private	function generate_random_code($len, $type="mix")
	{

		$num="1234567890";

		$alpha="abcdefghijklmnopqrstuvwxyz";

		$mix="$alpha$num";

		$src=($type=="mix"? $mix : ($type=="alpha"? $alpha : $num));

		$src_len=strlen($src);
	
		$code = NULL;	
	
		for($i=0;$i<$len;$i++)
		{

			$code.=strval(substr($src, rand(0, $src_len-1), 1));

		}

		return strval($code);

	}	
	
}