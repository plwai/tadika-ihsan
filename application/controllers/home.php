<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
	}
	
	public function index()
	{
       
	  
	    $data['title'] 	= "Home";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('home');
		$this->load->view('template/footer');
		
	}
	
	
	
}
