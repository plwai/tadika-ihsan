<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	   

		$this->load->model('fee_model');
	}
	
	public function index()
	{
	    $data['title'] 	= "Invoice";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('invoice');
		$this->load->view('template/footer');
		
	}
	
	public function generate()
	{
		$vc = NULL;
		$month = $this->input->post('month');
		$year = $this->input->post('year');

		$data['content'] = $this->fee_model->get_invoice_data($month, $year);

		if($this->fee_model->check_fee_exist($month, $year) AND $this->fee_model->check_month($month, $year))
		{
			foreach($data['content'] as $item)
			{
				if($vc != $item['vc'])
				{	

					if($vc != NULL)
					{
						$info = array(
									'vc'          => $vc,
									'month'       => $year.'-'.$month,
									'balance_fee' => $amount,
									'total_fee'   => $amount
								);
						$this->fee_model->insert_fee($info);
					}
					$amount = 0;
					$vc = $item['vc'];
				}
				$amount += $item['amount'];

				$checkout = explode(":", $item['check_out']);
				if($checkout[0] < 18)
				{
					$checkout[0] = 18;
					$checkout[1] = 0;
				}
				
				if($checkout[1] >= 30)
				{
					$amount = $amount + ((($checkout[0] - $item['end_time']) * 60 / 30)+1) * 5;
				}
				else
				{
					$amount = $amount + (($checkout[0] - $item['end_time']) * 60 / 30) * 5;
				}
			}

			$info = array(
						'vc'          => $vc,
						'month'       => $year.'-'.$month,
						'balance_fee' => $amount,
						'total_fee'   => $amount
					);
			$this->fee_model->insert_fee($info);
		}	

		$data['title'] 	= "Invoice";
	    
		$username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
		
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('invoice-form', $data);
		$this->load->view('template/footer');
	}
}