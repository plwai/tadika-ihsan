<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_record extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
	    $this->load->model('add_record_model');
	}
	
	public function teacher()
	{
	    $data['title'] 	= "TeacherRecord";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('add_teacher');
		$this->load->view('template/footer');
		
	}
	
	
	public function student()
	{
		$data['title'] 	= "StudentRecord";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('add_student');
		$this->load->view('template/footer');
	}

	public function add_student()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$now = time();
			$date = date ("Y-m-d", $now);
			$time = date ("G:i:s", $now);

			$secondGuardian = "";

			$studentInfo = array(	
								'name'          => $this->input->post("name"),
								'age'           => $this->input->post("age"),
								'gender'        => $this->input->post("gender"),
								'date_of_birth' => $this->input->post("date"),
								'street1'       => $this->input->post("guardianStreet1"),
								'street2'       => $this->input->post("guardianStreet2"),
								'postcode'      => $this->input->post("guardianPost"),
								'state'         => $this->input->post("guardianState"),
								'country'       => $this->input->post("guardianCountry"),
								'fee_id'        => $this->input->post("fee"),
								'regis_date'    => $date,
								'stop_date'     => "00:00:00",
								'discount'      => $this->input->post("discount"),
								'image'			=> ''
							);

			$date_of_birth = $this->input->post("guardianDate");
			$year = explode("-", $date_of_birth);
			$age = date("Y", $now) - $year[0];

			$guardianInfo = array(
								'relation'      => $this->input->post("guardianRelation"),
								'name'          => $this->input->post("guardianName"),
								'age'           => $age,
								'date_of_birth' => $this->input->post("guardianDate"),
								'ic_num'        => $this->input->post("guardianIdentity"),
								'occupation'    => $this->input->post("guardianOccupation"),
								'contact'       => $this->input->post("guardianContact"),
								'street1'       => $this->input->post("guardianStreet1"),
								'street2'       => $this->input->post("guardianStreet2"),
								'postcode'      => $this->input->post("guardianPost"),
								'state'         => $this->input->post("guardianState"),
								'country'       => $this->input->post("guardianCountry"),
								'nationality'   => $this->input->post("guardianNationality"),
							);
								

			if($this->input->post("guardianName2") != "")
			{

				$date_of_birth = $this->input->post("guardianDate2");
				$year = explode("-", $date_of_birth);
				$age = date("Y", $now) - $year[0];

				$secondGuardian = array(
										'relation'      => $this->input->post("guardianRelation"),
										'name'          => $this->input->post("guardianName2"),
										'age'           => $age,
										'date_of_birth' => $this->input->post("guardianDate2"),
										'ic_num'        => $this->input->post("guardianIdentity2"),
										'occupation'    => $this->input->post("guardianOccupation2"),
										'contact'       => $this->input->post("guardianContact2"),
										'street1'       => $this->input->post("guardian2Street1"),
										'street2'       => $this->input->post("guardian2Street2"),
										'postcode'      => $this->input->post("guardianPost2"),
										'state'         => $this->input->post("guardianState2"),
										'country'       => $this->input->post("guardianCountry2"),
										'nationality'   => $this->input->post("guardianNationality2"),
								  );
				
			}

			$this->add_record_model->add_record($studentInfo, $guardianInfo, $secondGuardian);
		}
		header("Location: ".base_url()."add_record/student");
	}

	public function add_teacher()
	{
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$now = time();
			$date = date ("Y-m-d", $now);
			$time = date ("G:i:s", $now);

			$teacherInfo = array(	
								'name'               => $this->input->post("name"),
								'age'                => $this->input->post("age"),
								'gender'             => $this->input->post("gender"),
								'street1'            => $this->input->post("street1"),
								'street2'            => $this->input->post("street2"),
								'postcode'           => $this->input->post("poscode"),
								'state'              => $this->input->post("state"),
								'country'            => $this->input->post("country"),
								'nationality'        => $this->input->post("nationality"),
								'contact'            => $this->input->post("contact"),
								'email'              => $this->input->post("email"),
								'ic_num'             => $this->input->post("icNum"),
								'start_work_date'    => $date,
								'resign_date'        => "------",
								'image'			     => ''
							);

			$this->add_record_model->add_teacher($teacherInfo);
		}
		header("Location: ".base_url()."add_record/teacher");
	}
}