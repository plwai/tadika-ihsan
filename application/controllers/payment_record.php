<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_record extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
	    $this->load->model('payment_record_model');
	}
	
	public function index()
	{
	    $data['title'] 	= "Payment record";
	    
	    if(date('n') == 1)
	    {
	    	$new_data = 12;
	    }
	    else
	    {
	    	$new_data = date('n') - 1;
	    }

	    if(date('n') == 1)
	    {
	    	$data_year = date('Y') - 1;
	    }
	    else
	    {
	    	$data_year = date('Y');
	    }

	    $username = $this->session->userdata('username');
	    
	    $data['username'] = $username;
	    
	    $data['result'] = $this->payment_record_model->get_fee_record($new_data, $data_year);

	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('payment_record');
		$this->load->view('template/footer');
		
	}

	public function get_data()
	{
		$count = 0;
		$month = $this->input->post('month');
		$year = $this->input->post('year');

		$result = $this->payment_record_model->get_fee_record($month, $year);

		$data['contents'] = "<thead>
								<tr>
									<th>No</th>
									<th>NAME</th>
									<th>VC</th>
									<th>TOTAL FEE</th>
									<th>BALANCE FEE</th>
									<th>Details</th>
								</tr>
							</thead>
							<tbody>";

		foreach($result as $item)
		{
			$count++; // counter

			$data['contents'] .= "<tr>
									<th>".$count."</th>
									<th>".$item['name']."</th>
									<th>".$item['vc']."</th>
									<th>".$item['total_fee']."</th>
									<th>".$item['balance_fee']."</th>
									<th><a class='pointer' onclick='showDetails(".$item['vc'].")'>Details</a></th>
								  </tr>";
		}

		$data['contents'] .= "	</tbody>
							</table>";

		echo json_encode($data);
	}

	public function get_details()
	{
		$count = 0;

		$vc = $this->input->post('vc');
		$month = $this->input->post('month');
		$year = $this->input->post('year');

		$result = $this->payment_record_model->get_details($vc, $month, $year);

		$data['contents'] = "<br><br>
							<div class='row font_bold'>
								<div class='col-sm-2'>
									vc number: 
								</div>
								<div class='col-sm-6'>
									".$result[0]['vc']."
								</div>
							</div>
							<div class='row font_bold'>
								<div class='col-sm-2'>
									Name: 
								</div>
								<div class='col-sm-6'>
									".$result[0]['student_name']."
								</div>
							</div>
							<div class='empty_box'>
								
							</div>
							<div class='row'>
								<table class='table'>
									<thead>
										<tr>
											<th style='width:200px;'>#</th>
											<th>Pay date</th>
											<th>Amount(RM)</th>
										</tr>
									</thead>
									<tbody>";

		foreach($result as $item)
		{
			$count++;
			$data['contents'] .= "<tr>
									<th>".$count."</th>
									<th>".$item['pay_date']."</th>
									<th>".number_format($item['paid_fee'], 2, '.', '')."</th>
								  </tr>";
		}

		$amount = $result[0]['total_fee'] - $result[0]['balance_fee'];

		$data['contents'] .= "    <tr>
									<th></th>
									<th></th>
									<th>Total paid : ".number_format($amount, 2, '.', '')."<br><br>Balance fee: ".number_format($result[0]['balance_fee'], 2, '.', '')."</th>
								  </tr>
							  </tbody>
							</table>
							<div class='col-sm-offset-4'>
							<button onclick='javascript:location.reload()' class='btn btn-default btn-lg'>Back</button>
							";
	    
	    echo json_encode($data);
	}
}