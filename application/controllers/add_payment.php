<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_payment extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
	    $this->load->model('fee_model');
	    $this->load->model('attendance_model');
	}
	
	public function index()
	{
	    $data['title'] 	= "Add Payment";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('add_payment');
		$this->load->view('template/footer');
		
	}

	public function check_min()
	{
		$vc = $this->input->post("vc");
		$month = $this->input->post("month");
		$amount = $this->input->post("amount");
		$year = $this->input->post("year");
		$data['result'] = $this->fee_model->check_min($amount, $vc, $year."-".$month);

		echo json_encode($data);
	}

	// Return true if valid payment number is filled in
	public function check_amount()
	{
		$amount = $this->input->post('payvalue');
		if($amount <= 0)
		{
			echo json_encode("Invalid input.");
		}
		else if(!preg_match('#^\d+(\.(\d{2}))?$#', $amount))
		{
			echo json_encode("It should be only 2 decimal places");
		}
		else
		{
			echo json_encode("true");
		}
	}

	public function pay_fee()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST')
		{
			$data['title'] 	= "Add Payment";
		    
		    $username = $this->session->userdata('username');
		    
		    $data['username'] 	= $username;

			$vc = $this->input->post("vcno");
			$month = $this->input->post("month");
			$amount = $this->input->post("payvalue");
			$year = $this->input->post("year");

			$data['content'] = $this->fee_model->pay_fee($amount, $vc, $year."-".$month, $this->session->userdata('staff_id'));
			$this->load->view('template/header', $data);
			$this->load->view('template/sidebar', $data);
			$this->load->view('receipt');
			$this->load->view('template/footer');
		}
		else
		{
			$data['title'] 	= "Add Payment";
		    
		    $username = $this->session->userdata('username');
		    
		    $data['username'] 	= $username;
		    
		    $this->load->view('template/header', $data);
			$this->load->view('template/sidebar', $data);
			$this->load->view('add_payment');
			$this->load->view('template/footer');
		}
	}

	public function check_vc()
	{

		$vc = $this->input->post('vcno');
		if ($this->attendance_model->check_vc($vc))
		{
			echo json_encode("true"); 
		} 
		else 
		{
			echo json_encode("The vc is not existed.");
		}
	}
}