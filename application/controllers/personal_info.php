<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personal_info extends CI_Controller 
{

	public function __construct(){
	    parent::__construct();
	    if(!$this->session->userdata('is_logged_in')){
			redirect('login','refresh');
			die();
	    }	    
		$this->load->model('personal_model');
	}
	
	public function teacher()
	{
	    $data['title'] 	= "Teacher Information";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
	    
		$data['result'] = $this->personal_model->get_teacher();
		
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('teacher_info');
		$this->load->view('template/footer');
		
	}
	
	
	public function student()
	{
		$data['title'] 	= "Student Information";
	    
	    $username = $this->session->userdata('username');

	    $data['result'] = $this->personal_model->get_student();
	    
	    $data['username'] 	= $username;
	    
	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('student_info');
		$this->load->view('template/footer');
	}
	
	public function studentdetails($id)
	{
		$data['title'] 	= "Student Information";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
		
	    $data['student_result'] = $this->personal_model->get_student($id);
	    $data['parent_result'] = $this->personal_model->get_guardian($id);
	    $data['package_result'] = $this->personal_model->get_package();

	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('student_details');
		$this->load->view('template/footer');
	}
	
	public function teacherdetails($id)
	{
		$data['title'] 	= "Teacher Information";
	    
	    $username = $this->session->userdata('username');
	    
	    $data['username'] 	= $username;
		$data['id'] = $id;
	    
	    $data['result'] = $this->personal_model->get_teacher($id);

	    $this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('teacher_details');
		$this->load->view('template/footer');
	}
}