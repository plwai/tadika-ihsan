<!-- css files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/print_teacher_details.css">

<div class="container-fluid">
	<div class="row">
		<div class="col-md-7 col-md-offset-4">
			<h1>Teacher Infomation</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-1 col-md-11">
			<h2>Teacher Profile</h2>
			<hr>
			<div class="row">
				<div class="col-md-offset-4 col-md-3">
					<?php if($result[0]['image'] != NULL) {?>
					<img src="<?php echo base_url()."assets/image/teacher/".$result[0]['image'];?>" width="140" height="200">
					<?php
						}
					 else{ ?>
					<img src="<?php echo base_url()."assets/image/teacher/empty.png";?>" width="140" height="200">
					<?php } ?>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4 print_field">
							Id: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['staff_id']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Name: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['name']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Age: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['age']; ?> 
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Gender: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['gender']; ?> 
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Identity Card No/Passport No: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['ic_num']; ?> 
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Address: 
						</div>
						<div class="col-md-8 address_box">
							<?php echo $result[0]['street1'].", <br>".$result[0]['street2'].", <br>".$result[0]['postcode'].", <br>".$result[0]['state'].", <br>".$result[0]['country']; ?>
						</div>
					</div>
					<br>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4 print_field">
							Contact Number: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['contact']; ?>  
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Email: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['email']; ?> 
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Nationality: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['nationality']; ?>  
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Start Working Date: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['start_work_date']; ?> 
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Resign Date: 
						</div>
						<div class="col-md-8">
							<?php echo $result[0]['resign_date']; ?>  
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-offset-4 col-md-3 button_box">
		<button type="button" class="btn btn-default btn-lg" onclick="javascript:window.print()">
			<span class="glyphicon glyphicon-download-alt"></span> Report Generate
		</button>
	</div>

</div>