<!DOCTYPE html>
<!-- ajax control js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/selecter/invoice_select.js"></script>

<!-- Jquery Validation Plugin version 1.13.0-->
<script src="<?php echo base_url(); ?>assets/jquery-validation-1.13.1/dist/jquery.validate.js"></script>

<div class="container-fluid">
	<?php echo form_open('add_payment/pay_fee'); ?>
	<div class="col-sm-6 col-md-4">
		<h3>Add Payment</h2>
		<hr>
		<div class="form-group">
			<h4>Student VC No</h4>
			<input type="text" name="vcno" class="form-control" id="vcno">
		</div>
		<div class="form-group">
			<h4>Payment For Month</h4>
			<div class="col-sm-6">
				
				<select name="month" id="month_dropdown" class="form-control">
					<?php if(date('n')>1 OR date('n') == 1) { ?><option id="jan" value="1" <?php echo date('n')==2?"selected":"" ?>>Jan</option><?php } ?>
					<?php if(date('n')>2 OR date('n') == 1) { ?><option id="feb" value="2" <?php echo date('n')==3?"selected":"" ?>>Feb</option><?php } ?>
					<?php if(date('n')>3 OR date('n') == 1) { ?><option id="mar" value="3" <?php echo date('n')==4?"selected":"" ?>>Mar</option><?php } ?>
					<?php if(date('n')>4 OR date('n') == 1) { ?><option id="apr" value="4" <?php echo date('n')==5?"selected":"" ?>>Apr</option><?php } ?>
					<?php if(date('n')>5 OR date('n') == 1) { ?><option id="may" value="5" <?php echo date('n')==6?"selected":"" ?>>May</option><?php } ?>
					<?php if(date('n')>6 OR date('n') == 1) { ?><option id="june" value="6" <?php echo date('n')==7?"selected":"" ?>>Jun</option><?php } ?>
					<?php if(date('n')>7 OR date('n') == 1) { ?><option id="july" value="7" <?php echo date('n')==8?"selected":"" ?>>July</option><?php } ?>
					<?php if(date('n')>8 OR date('n') == 1) { ?><option id="aug" value="8" <?php echo date('n')==9?"selected":"" ?>>Aug</option><?php } ?>
					<?php if(date('n')>9 OR date('n') == 1) { ?><option id="sep" value="9" <?php echo date('n')==10?"selected":"" ?>>Sep</option><?php } ?>
					<?php if(date('n')>10 OR date('n') == 1) { ?><option id="oct" value="10" <?php echo date('n')==11?"selected":"" ?>>Oct</option><?php } ?>
					<?php if(date('n')>11 OR date('n') == 1) { ?><option id="nov" value="11" <?php echo date('n')==12?"selected":"" ?>>Nov</option><?php } ?>
					<?php if(date('n') == 1){ ?><option id="dec" value="12" <?php echo date('n')==1?"selected":"" ?>>Dec</option><?php } ?>
				</select>
			</div>
			<div class="col-sm-6">
				<select class="form-control" id="class_dropdown" name="year">
					<?php 
						for($i = 2012; $i <= date('Y'); $i++)
						{
							if($i == date('Y') AND date('n') != 1)
							{
								echo "<option value='".$i."' selected>".$i."</option>";
							}
							else if($i == date('Y') - 1 AND date('n') == 1)
							{
								echo "<option value='".$i."' selected>".$i."</option>";
								break;
							}
							else
							{
								echo "<option value='".$i."'>".$i."</option>";
							}
						}
					?>
				</select>
			</div>
		</div>
		<br>
		<br>

		<div class="form-group">
			<h4>Amount To Pay (RM)</h4>
			<input type="text" name="payvalue" class="form-control" id="payvalue" autocomplete='off'>
		</div>


		<br>
		
		<button type="submit" class="btn btn-default">Submit and Print Receipt</button>
		
		<br>
		<br>
		<br>
		<br>
		
	<?php echo validation_errors(); ?>
	<?php echo $this->session->flashdata('errmsg'); ?>
	<?php echo form_close(); ?>
	</div>

	<div class="col-sm-offset-6 col-md-offset-8">
	</div>
</div>
	
<!-- Register validation javascript -->
<script src="<?php echo base_url(); ?>assets/javascript/validate/addpayment-validate.js"></script>