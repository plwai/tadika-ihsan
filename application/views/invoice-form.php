<?php 
$_list = array();
$vc = "";
$amount = 0;
$count = 0;
$checkout = array();
$overtime = 0;
$attendance = "<div class='row'>
					<table class='table' style='width: 400px;'>
						<thead>
							<tr>
								<th>#</th>
								<th>Date</th>
								<th>Check out(Time)</th>
								<th>Overtime</th>
							</tr>
						</thead>
						<tbody>";

if(empty($content))
{
	$_list['info'] = "<div class='col-sm-offset-3 col-sm-5'>
						<div class='col-sm-offset-1'>
							<h1>No Data Available</h1>
						</div>
					  </div>";
}

	foreach($content as $item)
	{
		
		if($vc != $item['vc'])
		{
			if($vc !="")
			{
				$_list[$vc] .= '<tr>
									<th>1</th>
									<th>Basic fee</th>
									<th>'.number_format($item['amount'], 2, '.', '').'</th>
								</tr>';
			}
			
			if($amount != 0 && $vc != "")
			{
				$cost = $amount;
				
				$_list[$vc] .= '<tr>
									<th>2</th>
									<th>Overtime '.$overtime.' hours</th>
									<th>'.number_format($cost, 2, '.', '').'</th>
								</tr>
								<br>';

				$attendance = "<h3>Overtime Details</h3>".$attendance;
				$attendance .= '</tbody>
								</table>
								</div>';
			}
			else
			{
				$attendance = "";
			}
			
			if($vc != "")
			{
				$total = $amount + $basic;

				$_list[$vc] .= '<tr>
									<th></th>
									<th></th>
									<th>Total '.number_format($total, 2, '.', '').'</th>
								</tr>
								</tbody>
								</table>
								</div>'.$attendance.'
								<div class="page_break"></div>';
			}
			$amount = 0;
			$count = 0;

			$attendance = "<div class='row'>
					<table class='table' style='width: 400px;'>
						<thead>
							<tr>
								<th>#</th>
								<th>Date</th>
								<th>Check out(Time)</th>
								<th>Overtime</th>
							</tr>
						</thead>
						<tbody>";

			$vc = $item['vc'];
			$_list[$vc] = '<br>
							<br>
							<div class="row">
								<div class="col-md-offset-1 col-md-3 print_logo">
									<h1>Tadika Ihsan</h1>
								</div>
								<div class="col-md-offset-3 col-md-5 print_address">
									Tadika Ihsan <br>
									5, Jalan Ihsan 10,<br>
									Universiti Teknologi Malaysia,<br>
									12111 Skudai,<br>
									Johor.<br>
								</div>
							</div>

							<hr>
							
							<br>
							<div class="row">
								<div class="col-md-offset-1 col-md-3 print_logo">
									Student Name: '.$item['studentname'].'<br>
									Student Number: '.$item['vc'].'<br>
									Type: '.$item['name'].'<br>
								</div>
								<div class="col-md-offset-3 col-md-5 print_address">
									<div class="row">
										<div class="col-md-12">
											<span class="print_id">Invoice ID:</span><span class="print_id_value">AS12344</span>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<span class="print_id">Invoice Date:</span><span class="print_id_value">1-12-2014</span>
										</div>
									</div>
								</div>
							</div>
							<div class="empty_box">
								
							</div>
							<div class="row">
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Description</th>
											<th>Amount(RM)</th>
										</tr>
									</thead>
									<tbody>
							';
			$basic = $item['amount'];
			$overtime = 0;
		}
		
		$checkout = explode(":", $item['check_out']);
		if($checkout[0] < 18)
		{
			$checkout[0] = 18;
			$checkout[1] = 0;
		}
		$overtime += ($checkout[0] - $item['end_time']);
		
		if($checkout[1] >= 30)
		{
			$amount = $amount + ((($checkout[0] - $item['end_time']) * 60 / 30)+1) * 5;
			$overtime += 0.5;
		}
		else
		{
			$amount = $amount + (($checkout[0] - $item['end_time']) * 60 / 30) * 5;
		}

		if($checkout[0] >= 18)
		{
			if($checkout[0] == 18 AND $checkout[1] >= 30)
			{
				$count++;
				$attendance .= "<tr>
									<th>".$count."</th>
									<th>".$item['date']."</th>
									<th>".$item['check_out']."</th>
									<th>".$checkout[1]." minutes</th>
								</tr>";
			}
			else if($checkout[0] > 18)
			{
				$count++;
				$attendance .= "<tr>
									<th>".$count."</th>
									<th>".$item['date']."</th>
									<th>".$item['check_out']."</th>
									<th>".$checkout[0]." hours ".$checkout[1]." minutes</th>
								</tr>";
			}
		}
	}

	if(!empty($content))
	{
		$_list[$vc] .= '<tr>
							<th>1</th>
							<th>Basic fee</th>
							<th>'.number_format($item['amount'], 2, '.', '').'</th>
						</tr>';
		
		if($amount != 0)
		{
			$cost = $amount;
			
			$_list[$vc] .= '<tr>
								<th>2</th>
								<th>Overtime '.$overtime.' hours</th>
								<th>'.number_format($cost, 2, '.', '').'</th>
							</tr>';

			$attendance = "<h3>Overtime Details</h3>".$attendance;
			$attendance .= '</tbody>
							</table>
							</div>';
		}
		else
		{
			$attendance = "";
		}
		
		$total = $amount + $basic;

		$_list[$vc] .= '<tr>
							<th></th>
							<th></th>
							<th>Total '.number_format($total, 2, '.', '').'</th>
						</tr>
						</tbody>
						</table>
						</div>'.$attendance.'';
	}
?>

<!-- css files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/print_invoice.css">

<div class="row">
	<div class="col-md-12">
		<div class="row">
		
		<div class="col-md-offset-4 col-md-3">
			<button onclick="javascript:window.print()" type="button" class="btn btn-default btn-lg">
				<span class="glyphicon glyphicon-download-alt"></span> Generate Invoice
			</button>
		</div>
		</div>
		<?php
			foreach($_list as $item)
			{
				echo $item;
			}
		?>
		<div class="col-md-offset-4 col-md-3">
			<button onclick="javascript:window.print()" type="button" class="btn btn-default btn-lg">
				<span class="glyphicon glyphicon-download-alt"></span> Generate Invoice
			</button>
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
	</div>
</div>