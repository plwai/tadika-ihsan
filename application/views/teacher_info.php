<!DOCTYPE html>

<?php
	$_list = "";
	
	foreach($result as $item)
	{
		$_list .= "<tr>
					<th>".$item['staff_id']."</th>
					<th>".$item['name']."</th>
					<th>".$item['age']."</th>
					<th>".$item['gender']."</th>
					<th>".$item['contact']."</th>
					<th>".$item['email']."</th>
					<th><a href='teacherdetails/".$item['staff_id']."'>Details</a></th>
				  </tr>
					";
	}
?>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables-1.10.4/media/css/jquery.dataTables.css">

<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<div class="container-fluid">
	<h1>Teacher Infomation</h1>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Staff ID</th>
				<th>NAME</th>
				<th>AGE</th>
				<th>GENDER</th>
				<th>CONTACT</th>
				<th>EMAIL </th>
				<th>MORE DETAILS</th>
		</thead>
		<tbody>
			<?php echo $_list; ?>

		</tbody>
	</table>
</div>

<script>
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>

<!-- Register validation javascript -->
<script src="<?php echo base_url(); ?>assets/javascript/validate/register-validate.js"></script>