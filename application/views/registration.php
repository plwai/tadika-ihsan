<!DOCTYPE html>

<!-- Jquery Validation Plugin version 1.13.0-->
<script src="<?php echo base_url(); ?>assets/jquery-validation-1.13.1/dist/jquery.validate.js"></script>

<div class="container-fluid">
	<?php echo form_open('registration/register'); ?>
	<div class="col-sm-6 col-md-4">
		<h3>Registration</h2>
		<hr>
		<div class="form-group">
			<h4>Email</h4>
			<input type="email" name="email" class="form-control" id="email">
		</div>
		<div class="form-group">
			<h4>Name</h4>
			<input type="text" name="name" class="form-control" id="name">
		</div>
		<div class="form-group">
			<h4>Password</h4>
			<input type="password" name="password" class="form-control" id="password">
		</div>
		<div class="form-group">
			<h4>Confirm Password</h4>
			<input type="password" name="confirmPass" class="form-control" id="confirmPass">
		</div>
		<br>
		<button type="submit" class="btn btn-default">Submit</button>
		
	<?php echo validation_errors(); ?>
	<?php echo $this->session->flashdata('errmsg'); ?>
	<?php echo form_close(); ?>
	</div>

	<div class="col-sm-offset-6 col-md-offset-8">
	</div>
</div>
	
<!-- Register validation javascript -->
<script src="<?php echo base_url(); ?>assets/javascript/validate/register-validate.js"></script>