<!-- Bootstrap 3.2.0 -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-theme.min.css">

<!-- Jquery -->
<script src="<?php echo base_url(); ?>assets/jquery/jquery-2.1.1.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

<!-- css file -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/template/template.css">

<?php
	if($title != "Login")
	{
		$headerClass = "collapse header-collapse";
	}
	else
	{
		$headerClass = "";
	}
?>

<head>
	<title><?php echo $title; ?></title>
	<div class="container-fluid header <?php echo $headerClass; ?>">
		<div class="col-sm-3 col-md-2">
			<a href="<?php echo base_url(); ?>"><h1 class="header">e-Ihsan</h1></a>
		</div>
		<div class="col-sm-offset-5 col-sm-4 col-md-offset-8 col-md-2">
			<?php 
				if(isset($username))
				{
					echo "<p class='header'>Welcome ".$username."</p>";
					echo "<a href='".base_url()."login/logout' class='header'>logout</a>";
				}
			?>
		</div>
	</div>
	
	<div class="wrapper">
</head>