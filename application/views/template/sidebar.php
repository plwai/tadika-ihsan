<?php
	$record_open = ".record-menu";
	$personal_open = ".personal-menu";
	
	if($title == "TeacherRecord")
	{
		$record_open = "";
	}
	else if($title == "StudentRecord")
	{
		$record_open = "";
	}
	
	if($title == "Teacher Information")
	{
		$personal_open = "";
	}
	else if($title == "Student Information")
	{
		$personal_open = "";
	}
?>

<!-- css file -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/template/sidebar.css">

<body>
  <div class="container-fluid">
	<div class="row">
    <div class="col-md-2 col-sm-3 sidebar">
		<nav class="navbar navbar-default" role="navigation">
			<div class="collapse in logo-collapse">
				<a href="<?php echo base_url(); ?>"><span class="logo">e-Ihsan</span></a>
				
			</div>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse">
				<ul class="nav nav-stacked">
					<li class="<?php echo $title=="Home"?"active":""; ?>">
					    <a href="<?php echo base_url()."home";?>" class="<?php echo $title=="Home"?"active":""; ?>">Home</a>
					</li>
					<li class="<?php echo $title=="Attendance"?"active":""; ?>">
						<a class="<?php echo $title=="Attendance"?"active":""; ?>" href="<?php echo base_url()."attendance";?>">Attendance</a>
					</li>
					<li class="<?php echo $title=="Attendance record"?"active":""; ?>">
						<a class="<?php echo $title=="Attendance record"?"active":""; ?>" href="<?php echo base_url()."attendance_record";?>">Attendance Record</a>
					</li>
					<li><a class="pointer" data-toggle="collapse" data-target="<?php echo $personal_open; ?>">Personal Information<span class="caret"></span></a>
						<ul class="nav nav-stacked <?php echo $title=="Teacher Information"?"in":""; echo $title=="Student Information"?"in":"";?> collapse personal-menu">
							<li class="personal-menu <?php echo $title=="Teacher Information"?"active":""; ?>">
								<a class="record-menu <?php echo $title=="Teacher Information"?"active":""; ?>" href="<?php echo base_url()."personal_info/teacher";?>">Teacher Info</a>
							</li>
							<li class="personal-menu <?php echo $title=="Student Information"?"active":"" ?>">
								<a class="record-menu <?php echo $title=="Student Information"?"active":"" ?>" href="<?php echo base_url()."personal_info/student";?>">Student Info</a>
							</li>
						</ul>
					</li>
					<li class="<?php echo $title=="Invoice"?"active":""; ?>">
						<a href="<?php echo base_url()."invoice";?>" class="<?php echo $title=="Invoice"?"active":""; ?>">Invoice</a>
					</li>
					<li class="<?php echo $title=="Add Payment"?"active":""; ?>">
						<a href="<?php echo base_url()."add_payment";?>" class="<?php echo $title=="Add Payment"?"active":""; ?>">Add Payment</a>
					</li>
					<li class="<?php echo $title=="Payment record"?"active":""; ?>">
						<a href="<?php echo base_url()."payment_record";?>" class="<?php echo $title=="Payment record"?"active":""; ?>">Payment Record</a>
					</li>
					<li><a class="pointer" data-toggle="collapse" data-target="<?php echo $record_open ?>">Add Record<span class="caret"></span></a>
						<ul class="nav nav-stacked <?php echo $title=="TeacherRecord"?"in":""; echo $title=="StudentRecord"?"in":"";?> collapse record-menu">
							<li class="record-menu <?php echo $title=="TeacherRecord"?"active":""; ?>">
								<a class="record-menu <?php echo $title=="TeacherRecord"?"active":""; ?>" href="<?php echo base_url()."add_record/teacher";?>">Teacher Record</a>
							</li>
							<li class="record-menu <?php echo $title=="StudentRecord"?"active":"" ?>">
								<a class="record-menu <?php echo $title=="StudentRecord"?"active":"" ?>" href="<?php echo base_url()."add_record/student";?>">Student Record</a>
							</li>
						</ul>
					</li>
					<li class="<?php echo $title=="Overview"?"active":""; ?>">
						<a class="<?php echo $title=="Overview"?"active":""; ?>" href="<?php echo base_url()."overview";?>">Overview</a>
					</li>
					<li class="<?php echo $title=="Registration"?"active":""; ?>">
						<a class="<?php echo $title=="Registration"?"active":""; ?>" href="<?php echo base_url()."registration";?>">Registration</a>
					</li>
					<li><a href="<?php echo base_url()."login/logout";?>">Logout</a></li>              
			</div>
		</nav>
	</div>
	
	<!--Start of content-->
	<div class="col-md-10 col-sm-9 content">