<!-- css files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/print_teacher_details.css">

<div class="container-fluid">
	<div class="row">
		<div class="col-md-offset-4 col-md-8">
			<h1>Student Information</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-1 col-md-11">
			<h2>Student Profile</h2>
			<hr>
			<div class="row">
				<div class="col-md-offset-4 col-md-3">
					<?php if($student_result[0]['image'] != NULL) {?>
					<img src="<?php echo base_url()."assets/image/student/".$student_result[0]['image'];?>" width="140" height="200">
					<?php
						}
					 else{ ?>
					<img src="<?php echo base_url()."assets/image/student/empty.png";?>" width="140" height="200">
					<?php } ?>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4 print_field">
							Name: 
						</div>
						<div class="col-md-8">
							<?php echo $student_result[0]['name']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							VC: 
						</div>
						<div class="col-md-8">
							<?php echo $student_result[0]['vc']; ?> 
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Age:  
						</div>
						<div class="col-md-8">
							<?php echo $student_result[0]['age']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Gender: 
						</div>
						<div class="col-md-8">
							<?php echo $student_result[0]['gender']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Address:
						</div>
						<div class="col-md-8 address_box">
							<?php echo $student_result[0]['street1'].", <br>".$student_result[0]['street2'].", <br>".$student_result[0]['postcode'].", <br>".$student_result[0]['state'].", <br>".$student_result[0]['country']; ?>
						</div>
					</div>
					<br>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4 print_field">
							Date of birth: 
						</div>
						<div class="col-md-8">
							<?php echo $student_result[0]['date_of_birth']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Fee package: 
						</div>
						<div class="col-md-8">
							<?php echo $package_result[$student_result[0]['fee_id']]; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Register date:
						</div>
						<div class="col-md-8">
							<?php echo $student_result[0]['regis_date']; ?>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 print_field">
							Discount:
						</div>
						<div class="col-md-8">
							<?php 
								if($student_result[0]['discount'] == 1)
								{
									echo "Available";
								}
								else
								{
									echo "Not available";
								}
							?>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<div class="page_break_details"></div>
	<div class="row">
		<div class="col-md-offset-1 col-md-11">
			<h2>Student's Parents / Guardian Infomation</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-offset-1 col-md-11">
			<h3><?php echo $parent_result[0]['relation']; ?></h3>
			<hr>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4 print_field">
						Name: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['name']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Age:  
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['age']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Identity Card No: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['ic_num']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Address:
					</div>
					<div class="col-md-8 address_box">
						<?php echo $parent_result[0]['street1'].", <br>".$parent_result[0]['street2'].", <br>".$parent_result[0]['postcode'].", <br>".$parent_result[0]['state'].", <br>".$parent_result[0]['country']; ?>
					</div>
				</div>
				<br>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4 print_field">
						Date of birth: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['date_of_birth']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Occupation: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['occupation']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Contact Number:
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['contact']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Nationality: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[0]['nationality']; ?>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
	
	<br>
	
	<?php
		if(isset($parent_result[1]))
		{
	?>
	<div class="page_break_details"></div>
	<div class="row">
		<div class="col-md-offset-1 col-md-11">
			<h3><?php echo $parent_result[1]['relation']; ?></h3>
			<hr>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4 print_field">
						Name: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['name']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Age:  
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['age']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Identity Card No: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['ic_num']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Address:
					</div>
					<div class="col-md-8 address_box">
						<?php echo $parent_result[1]['street1'].", <br>".$parent_result[1]['street2'].", <br>".$parent_result[1]['postcode'].", <br>".$parent_result[1]['state'].", <br>".$parent_result[1]['country']; ?>
					</div>
				</div>
				<br>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-4 print_field">
						Date of birth: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['date_of_birth']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Occupation: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['occupation']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Contact Number:
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['contact']; ?>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4 print_field">
						Nationality: 
					</div>
					<div class="col-md-8">
						<?php echo $parent_result[1]['nationality']; ?>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="col-md-offset-4 col-md-3 button_box">
		<br>
		<button type="button" class="btn btn-default btn-lg" onclick="javascript:window.print()">
			<span class="glyphicon glyphicon-download-alt"></span> Report Generate
		</button>
	</div>
</div>