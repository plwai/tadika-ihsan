<!-- css files -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/print_receipt.css">

<br>
<br>

<div class="row">
	<div class="col-md-offset-1 col-md-3 print_logo">
		<h1>Tadika Ihsan</h1>
	</div>
	<div class="col-md-offset-3 col-md-5 print_address">
		Tadika Ihsan <br>
		5, Jalan Ihsan 10,<br>
		Universiti Teknologi Malaysia,<br>
		12111 Skudai,<br>
		Johor.<br>
	</div>
</div>

<hr>

<br>
<div class="row">
	<div class="col-md-offset-1 col-md-3 print_logo">
		To <?php echo $content['student_name']; ?><br>
		VC: <?php echo $content['vc']; ?><br>
		Type: <?php echo $content['fee_name']; ?> <br>
	</div>
	<div class="col-md-offset-3 col-md-5 print_address">
		<div class="row">
			<div class="col-md-12">
				<span class="print_id">Receipt Number:</span><span class="print_id_value"><?php echo $content['receipt_id']; ?></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<span class="print_id">Payment Date:</span><span class="print_id_value"><?php echo $content['pay_date']; ?></span>
			</div>
		</div>
	</div>
</div>

<div class="empty_box"></div>

<div class="row">
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Description</th>
				<th>Amount(RM)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>1</th>
				<th>Cash Receive</th>
				<th><?php echo number_format($content['paid_fee'], 2, '.', '') ?></th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th>Total RM <?php echo number_format($content['paid_fee'], 2, '.', '') ?></th>
			</tr>
		</tbody>
	</table>
</div>

<div class="col-md-offset-4 col-md-3 button_box">
	<button onclick="javascript:window.print()" type="button" class="btn btn-default btn-lg">
		<span class="glyphicon glyphicon-download-alt"></span> Print Receipt
	</button>
</div>

