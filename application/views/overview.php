<!--jqplot-->
<script class="include" type="text/javascript" src="<?php echo base_url(); ?>assets/jqPlot/jquery.jqplot.min.js"></script>

<!--jqplot horizontal bar plugin-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jqPlot/plugins/jqPlot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jqPlot/plugins/jqPlot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jqPlot/plugins/jqPlot.pointLabels.min.js"></script>

<!-- ajax control js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/selecter/overview_select.js"></script>

<style>
.jqplot-yaxis{
	left: -10px !important;
}
</style>

<div class="row">
	<div class="col-sm-offset-4 col-sm-2">
		<select class="form-control" id="class_dropdown">
			<option selected disabled>Select a class</option>
			<option>Class1</option>
			<option>Class2</option>
			<option>Class3</option>
		</select>
	</div>
</div>
<div id="chart2" style="width:800px; height:600px;"></div>

