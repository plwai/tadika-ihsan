<!DOCTYPE html>

<?php
	$_list = "";
	
	foreach($result as $item)
	{
		$_list .= "<tr>
					<th>".$item['vc']."</th>
					<th>".$item['name']."</th>
					<th>".$item['age']."</th>
					<th>".$item['gender']."</th>
					<th>".$item['date_of_birth']."</th>
					<th>".$item['regis_date']."</th>
					<th>".$item['stop_date']."</th>
					<th><a href='studentdetails/".$item['vc']."'>Details</a></th>
				  </tr>
					";
	}
?>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables-1.10.4/media/css/jquery.dataTables.css">

<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<div class="container-fluid">
	<h1>Student Infomation</h1>
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>VC</th>
				<th>NAME</th>
				<th>AGE</th>
				<th>GENDER</th>
				<th>DATE OF BIRTH</th>
				<th>REGISTER DATE </th>
				<th>STOP DATE </th>
				<th>DETAILS</th>
		</thead>
		<tbody>
			<?php echo $_list; ?>
		</tbody>
	</table>
</div>

<script>
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>