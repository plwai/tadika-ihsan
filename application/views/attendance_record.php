<?php
	$_list = "";
	$count = 0;
	$vc = NULL;
	$attendance = 0;

	foreach($result as $item)
	{
		if($item['vc'] != $vc)
		{
			if($vc != NULL)
			{
				$_list .= "    <th>".$attendance."/".$dayNum."</th>
							   <th><a class='pointer' onclick='showDetails(".$vc.")'>Details</a></th>
							</tr>";
			}

			$vc = $item['vc'];
			$attendance = 0;
			$count++;
			$_list .= "<tr>
						<th>".$count."</th>
						<th>".$item['name']."</th>
						<th>".$item['vc']."</th>";
		}

		$attendance++;
		
	}

	if(isset($result[0]))
	{
		$_list .= "    <th>".$attendance."/".$dayNum."</th>
					   <th><a class='pointer' onclick='showDetails(".$vc.")'>Details</a></th>
					</tr>";
	}
?>

<!DOCTYPE html>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables-1.10.4/media/css/jquery.dataTables.css">

<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>

<!-- ajax control js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/javascript/selecter/attendance_record_select.js"></script>

<div class="container-fluid">
	<h1>Attendance Record</h1>
	<div class="page_select">
		<div class="row">
			<div class="col-sm-2">
				<select name="month" id="month_dropdown" class="form-control">
					<?php if(date('n')>1 OR date('n') == 1) { ?><option id="jan" value="1" <?php echo date('n')==2?"selected":"" ?>>Jan</option><?php } ?>
					<?php if(date('n')>2 OR date('n') == 1) { ?><option id="feb" value="2" <?php echo date('n')==3?"selected":"" ?>>Feb</option><?php } ?>
					<?php if(date('n')>3 OR date('n') == 1) { ?><option id="mar" value="3" <?php echo date('n')==4?"selected":"" ?>>Mar</option><?php } ?>
					<?php if(date('n')>4 OR date('n') == 1) { ?><option id="apr" value="4" <?php echo date('n')==5?"selected":"" ?>>Apr</option><?php } ?>
					<?php if(date('n')>5 OR date('n') == 1) { ?><option id="may" value="5" <?php echo date('n')==6?"selected":"" ?>>May</option><?php } ?>
					<?php if(date('n')>6 OR date('n') == 1) { ?><option id="june" value="6" <?php echo date('n')==7?"selected":"" ?>>Jun</option><?php } ?>
					<?php if(date('n')>7 OR date('n') == 1) { ?><option id="july" value="7" <?php echo date('n')==8?"selected":"" ?>>July</option><?php } ?>
					<?php if(date('n')>8 OR date('n') == 1) { ?><option id="aug" value="8" <?php echo date('n')==9?"selected":"" ?>>Aug</option><?php } ?>
					<?php if(date('n')>9 OR date('n') == 1) { ?><option id="sep" value="9" <?php echo date('n')==10?"selected":"" ?>>Sep</option><?php } ?>
					<?php if(date('n')>10 OR date('n') == 1) { ?><option id="oct" value="10" <?php echo date('n')==11?"selected":"" ?>>Oct</option><?php } ?>
					<?php if(date('n')>11 OR date('n') == 1) { ?><option id="nov" value="11" <?php echo date('n')==12?"selected":"" ?>>Nov</option><?php } ?>
					<?php if(date('n') == 1){ ?><option id="dec" value="12" <?php echo date('n')==1?"selected":"" ?>>Dec</option><?php } ?>
				</select>
			</div>
			<div class="col-sm-2">
				<select class="form-control" id="class_dropdown" name="year">
					<?php 
						for($i = 2012; $i <= date('Y'); $i++)
						{
							if($i == date('Y') AND date('n') != 1)
							{
								echo "<option value='".$i."' selected>".$i."</option>";
							}
							else if($i == date('Y') - 1 AND date('n') == 1)
							{
								echo "<option value='".$i."' selected>".$i."</option>";
								break;
							}
							else
							{
								echo "<option value='".$i."'>".$i."</option>";
							}
						}
					?>
				</select>
			</div>
		</div>
		
		<br>
		
		<div class="row">
			<table id="table_id" class="display list_select">
				<thead>
					<tr>
						<th>No</th>
						<th>NAME</th>
						<th>STUDENT NUMBER</th>
						<th>NUMBER OF ATTENDANCE</th>
						<th>DETAILS</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $_list; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
$(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>