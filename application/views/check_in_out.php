<!-- Jquery Validation Plugin version 1.13.0-->
<script src="<?php echo base_url(); ?>assets/jquery-validation-1.13.1/dist/jquery.validate.js"></script>

<style>
	#vc_check_in-error{
		margin-left:90px;
	}
	
	#vc_check_out-error{
		margin-left:90px;
	}
</style>

<div class="container-fluid">
	<br>
	<br>
	<br>
	<div class="col-md-offset-3 col-md-5">
		<?php
			$attributes = array('class' => 'form-inline');
			echo form_open('attendance/'.$function, $attributes); 
		?>
		<div class="form-group">
			<label for="vc">VC number:</label>
			<input class="form-control" type="text" name="vc_<?php echo $function; ?>" id="vc_<?php echo $function; ?>">
			<button type="submit" class="btn btn-default btn-lg" style="margin-left:10px;">
				<?php echo $function=='check_in'?'Check In':'Check Out'; ?>
			</button>
			<span class="vcError"></span>
		</div>
		<?php echo validation_errors(); ?>
		<?php echo form_close(); ?>
	</div>
</div>

<!-- Attendance validation javascript -->
<script src="<?php echo base_url(); ?>assets/javascript/validate/attendance-validate.js"></script>