<div class="container-fluid">
	<div class="col-md-offset-3 col-md-5">
		<h3><?php echo $complete; ?></h3>
	</div>
	<br>
	<br>
	<br>
	<div class="col-md-offset-3 col-md-3">
		<?php echo form_open('attendance/check_in'); ?>
		<button type="submit" class="btn btn-default btn-lg" style="width:250px; margin-left:50px;">
			Check in
		</button>
		<?php echo validation_errors(); ?>
		<?php echo form_close(); ?>
		
		<?php echo form_open('attendance/check_out'); ?>
		<button type="submit" class="btn btn-default btn-lg" style="width:250px; margin-left:50px;">
			Check out
		</button>
		<?php echo validation_errors(); ?>
		<?php echo form_close(); ?>
	</div>
</div>