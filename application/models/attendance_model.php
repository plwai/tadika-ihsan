<?php

class Attendance_model extends CI_Model
{	
	public function check_in($vc, $staff_id)
	{
		$now = time();
		$date = date ("Y-m-d", $now);
		$time = date ("G:i:s", $now);
	
		$info = array(
			'vc'        => $vc,
			'date'      => $date,
			'staff_id'  => $staff_id,
			'check_in'  => $time,
			'check_out' => "00:00:00"
		);
		
		$this->db->insert('attendance', $info);
	}
	
	public function check_out($vc, $staff_id)
	{
		$now = time();
		$date = date ("Y-m-d", $now);
		$time = date ("G:i:s", $now);
	
		$info = array(
			'check_out' => $time
		);
		
		$this->db->where('date', $date);
		$this->db->where('vc', $vc);
		$this->db->update('attendance', $info);
	}
	
	public function check_vc($vc)
	{
		$this->db->where('vc', $vc);
		$query = $this->db->get('student');
		
		if($query->num_rows == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function check_in_duplicate($vc)
	{
		$now = time();
		$date = date ("Y-m-d", $now);
	
		$this->db->where('date', $date);
		$this->db->where('vc', $vc);
		$query = $this->db->get('attendance');
		
		if($query->num_rows == 1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function check_out_duplicate($vc)
	{
		$now = time();
		$date = date ("Y-m-d", $now);
	
		$this->db->where('date', $date);
		$this->db->where('vc', $vc);
		$query = $this->db->get('attendance');
		$data = $query->result_array();
		
		if($query->num_rows == 1)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function check_out_validate($vc)
	{
		$now = time();
		$date = date ("Y-m-d", $now);
	
		$this->db->where('date', $date);
		$this->db->where('vc', $vc);
		$query = $this->db->get('attendance');
		$data = $query->result_array();
		

		if($data[0]['check_out'] == "00:00:00")
		{
			return true;
		}
		
		return false;
		
	}

	public function get_data($month, $year)
	{
		$calc_date['start']=date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
		$calc_date['end']=date('Y-m-d', mktime(23, 59, 59, $month+1, 0, $year));

		$this->db->select('student.name, student.vc, attendance.check_in, attendance.check_out, attendance.date');
		$this->db->join('student', 'student.vc = attendance.vc');
		$this->db->where('date >=', $calc_date['start']);
		$this->db->where('date <=', $calc_date['end']);
		$query = $this->db->get("attendance");

		return $query->result_array();
	}

	public function get_details($vc, $month, $year)
	{
		$calc_date['start']=date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
		$calc_date['end']=date('Y-m-d', mktime(23, 59, 59, $month+1, 0, $year));

		$this->db->select('student.name, student.vc, attendance.check_in, attendance.check_out, attendance.date');
		$this->db->join('student', 'student.vc = attendance.vc');
		$this->db->where('date >=', $calc_date['start']);
		$this->db->where('date <=', $calc_date['end']);
		$this->db->where('attendance.vc', $vc);
		$query = $this->db->get("attendance");

		return $query->result_array();
	}
}
