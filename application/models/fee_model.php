<?php

class Fee_model extends CI_Model
{	
	public function get_invoice_data($month, $year)
	{
		$calc_date['start']=date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
		$calc_date['end']=date('Y-m-d', mktime(23, 59, 59, $month+1, 0, $year));
		
		$this->db->select('student.vc, student.name studentname, student.fee_id, attendance.check_in, attendance.check_out, attendance.date, fee_amount.*');
		$this->db->join('attendance', 'attendance.vc = student.vc');
		$this->db->join('fee_amount', 'fee_amount.fee_id = student.fee_id');
		$this->db->where('date >=', $calc_date['start']);
		$this->db->where('date <=', $calc_date['end']);
		$query = $this->db->get('student');
		
		return $query->result_array();
	}

	public function insert_fee($info)
	{
		$this->db->insert('fee_record', $info);
	}

	// Return false if the fee has already existed
	public function check_fee_exist($month, $year)
	{
		$date=$year.'-'.$month;
		$this->db->where('month', $date);
		$query = $this->db->get('fee_record');

		if($query->num_rows() > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function check_month($month, $year)
	{
		$now = time();
		$currentYear = date ("Y", $now);
		$currentMonth = date("m", $now);

		if($year <= $currentYear)
		{
			if($month >= $currentMonth)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	public function check_min($amount, $vc, $month)
	{
		$this->db->where("vc", $vc);
		$this->db->where("month", $month);
		$query = $this->db->get("fee_record");

		$data = $query->result_array();

		return $data[0]['balance_fee'];
	}

	public function pay_fee($amount, $vc, $month, $staff)
	{
		$now = time();
		$pay_time = date('Y-m-d', $now);

		$this->db->where("vc", $vc);
		$this->db->where("month", $month);
		$query = $this->db->get("fee_record");

		$data = $query->result_array();

		$record_id = $data[0]['record_id'];
		$balance = $data[0]['balance_fee'] - $amount;

		$info = array('balance_fee' => $balance);

		$this->db->where("vc", $vc);
		$this->db->where("month", $month);
		$this->db->update("fee_record", $info);

		$this->db->where("vc", $vc);
		$query = $this->db->get("student");

		$data = $query->result_array();

		$fee_id = $data[0]['fee_id'];
		$student_name = $data[0]['name'];

		$info = array(
					'vc'        => $vc,
					'record_id' => $record_id,
					'fee_id'    => $fee_id,
					'staff_id'  => $staff,
					'pay_date'  => $pay_time,
					'month'     => $month,
					'paid_fee'  => $amount
					);

		$this->db->insert('fee', $info);
		$last_id = $this->db->insert_id();

		//Get data for receipt

		$this->db->where('staff_id', $staff);
		$query = $this->db->get('teacher');


		$data = $query->result_array();

		$info['staff_name'] = $data[0]['name'];

		$this->db->where('fee_id', $fee_id);
		$query = $this->db->get('fee_amount');

		$data = $query->result_array();

		$info['fee_name'] = $data[0]['name'];
		$info['student_name'] = $student_name;
		$info['receipt_id'] = $last_id;

		return $info;
	}
}
