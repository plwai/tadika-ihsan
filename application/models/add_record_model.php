<?php

class Add_record_model extends CI_Model
{	
	public function add_record($studentInfo, $guardianInfo, $secondGuardianInfo = "")
	{
		$this->db->insert('student', $studentInfo);
		$last_id = $this->db->insert_id();

		$guardianInfo['vc'] = $last_id;

		$this->db->insert('parents', $guardianInfo);

		if($secondGuardianInfo != "")
		{
			$secondGuardianInfo['vc'] = $last_id;

			$this->db->insert('parents', $secondGuardianInfo);
		}
	}

	public function add_teacher($teacherInfo)
	{
		$this->db->insert('teacher', $teacherInfo);
	}
}
