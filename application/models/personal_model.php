<?php

class Personal_model extends CI_Model
{	
	public function get_teacher($id = NULL)
	{
		if($id == NULL)
		{
			$this->db->select("*");
			$query = $this->db->get("teacher");
			
			return $query->result_array();
		}
		else
		{
			$this->db->where("staff_id", $id);
			$query = $this->db->get("teacher");

			return $query->result_array();
		}
	}

	public function get_student($id = NULL)
	{
		if($id == NULL)
		{
			$this->db->select("*");
			$query = $this->db->get("student");
			
			return $query->result_array();
		}
		else
		{
			$this->db->where("vc", $id);
			$query = $this->db->get("student");

			return $query->result_array();
		}
	}

	public function get_guardian($id)
	{
		$this->db->where("vc", $id);
		$query = $this->db->get("parents");

		return $query->result_array();
	}

	public function get_package()
	{
		$this->db->select("*");
		$query = $this->db->get("fee_amount");

		$data = $query->result_array();
		
		$package = array();

		foreach($data as $item)
		{
			$package[$item['fee_id']] = $item['name'];
		}

		return $package;
	}
}