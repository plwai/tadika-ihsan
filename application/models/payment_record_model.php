<?php

class Payment_record_model extends CI_Model
{	
	public function get_fee_record($month, $year)
	{
		$calc_date['start']=date('Y-n', mktime(0, 0, 0, $month, 1, $year));
		$calc_date['end']=date('Y-n', mktime(23, 59, 59, $month+1, 0, $year));

		$this->db->select('student.name, student.vc, fee_record.total_fee, fee_record.balance_fee');
		$this->db->join('student', 'student.vc = fee_record.vc');
		$this->db->where('month', $calc_date['start']);
		$query = $this->db->get("fee_record");

		return $query->result_array();
	}

	public function get_details($vc, $month, $year)
	{
		$calc_date['start']=date('Y-n', mktime(0, 0, 0, $month, 1, $year));

		$this->db->select('student.name student_name, student.vc, fee_record.total_fee, fee_record.balance_fee, fee.paid_fee, fee.pay_date');
		$this->db->join('student', 'student.vc = fee.vc');
		$this->db->join('fee_record', 'fee_record.record_id = fee.record_id');
		$this->db->where('fee.month', $calc_date['start']);
		$this->db->where('fee.vc', $vc);
		$query = $this->db->get("fee");

		return $query->result_array();
	}
}
