<?php

class Login_model extends CI_Model
{	
	// Get the specific user data for login purpose.
	public function get_users($username = "")
	{
		$this->db->where('username', $username);
		$query = $this->db->get('user');
		
		// Check the id whether is in the database
		if($query->num_rows == 1)
		{
			$row = $query->result_array();
			$data = array(
				'username'     => $row[0]['username'],
				'password'     => $row[0]['password'],
				'enc_password' => $row[0]['enc_password'],
				'staff_id'     => $row[0]['staff_id']
			);
		}
		
		return $data;
	}
	
	public function checkUser($username)
	{
		$this->db->where('username', $username);
		$query = $this->db->get('user');
		
		if($query->num_rows == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function register($info)
	{
		$this->db->insert('user', $info);
	}

}
